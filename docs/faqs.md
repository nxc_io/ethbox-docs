---
sidebar_position: 8
---

# FAQs

**I just sent a message, can I get my ether back?**

No, as the sender you cannot reclaim a message. The only way you can recoup ether is by the message being [bumped](/messages/bumping) out of the ethbox OR the owner [removing](/messages/removing) your message. However, you will only recieve a portion of your ether back, depending on the drip time of the ethbox.

To learn more about drip time, please read [here](/ethboxes/drip/how-it-works)

**Somebody sent a rude message to my ethbox, can I get rid of it?**

You can remove messages in your ethbox. You will only claim a portion the ether in the message, depending on the [drip](/ethboxes/drip/how-it-works) time of your ethbox. The rest will be refunded to the sender.

Read [this](/messages/removing) for more on removing messages.

**How can I make messages expire faster?**

You can change the [drip](/ethboxes/drip/how-it-works) time in your ethbox if it is empty and locked. You can empty your ethbox by removing all of your messages.

To learn more, check out [this](/ethboxes/drip/setting).
