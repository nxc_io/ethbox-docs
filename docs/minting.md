---
sidebar_position: 2
---

# Minting

You can mint an ethbox for free through our [app](https://ethbox.nxc.io) or [contract](https://etherscan.io/address/0x2b74fbe86367056ff07d0f0fc05539b1afb61038). Ethboxes can receive messages without being minted.

**Through the app...**

Head to the "mint" tab, scroll down and press mint my ethbox.

![Mint](../images/mint.png)

**Through the contract...**

You can mint through the contract by first heading to our [write contract](https://etherscan.io/address/0x2b74fbe86367056ff07d0f0fc05539b1afb61038#writeProxyContract).

Connect to web3, this is located above the contact functions.  
![Web3](../images/web3.png)

Then press write on `mintMyEthbox` and approve the transaction.
![Contract](../images/contract.png)

Since ethboxes can receive messages without being minted...

- You can send a message to any address.
- Any address can send a message to you without minting.
- If you have existing messages in your ethbox when you mint, you will claim all the available value. Read more about claiming [here](/messages/claiming).

**Implementation**

The `mintMyEthbox` function in our contract handles minting.

```
function mintMyEthbox() external onlyProxy {...}
```

The first thing we do is assign `msg.sender` to the variable `sender`. This saves gas.

```
address sender = msg.sender;
```

Next, we check if they have already minted using an `address => bool` mapping. If they haven't, we continue and set their `minted` value to `true`.

```
require(!minted[sender], "ethbox already minted");
minted[sender] = true;
```

Then we pack up their information with default settings. More about variable [packing](/packing) here. We start with default settings, but both ethbox [size](/ethboxes/size/how-it-works) and [drip](/ethboxes/drip/how-it-works) can be updated post minting.

```
packedEthboxInfo[sender] = packEthboxInfo(
    sender,
    defaultDripTime,
    defaultEthboxSize,
    false
);
```

Finally, we emit a transfer event and if the ethbox has existing messages, the sender claims them all.

```
emit Transfer(address(0), sender, uint256(uint160(sender)));
if (ethboxMessages[sender].length > 0) {
    claimAll();
}
```

For details about the claiming implementation, head [here](/messages/claiming).
