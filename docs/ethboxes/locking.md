---
sidebar_position: 3
---

# Locking

An ethbox can be locked at any time.

**General Information**

- Once an ethbox is locked it cannot receive messages
- If an ethbox is locked and has no messages, the owner can change their [drip](ethboxes/drip/setting) time.

**Why would I lock my ethbox?**

- You do not want to recieve any messages.
- You are preparing to change your [drip](ethboxes/drip/setting) time.

**Implementaion**

Ethboxes are not locked by default.

Just like drip time and size, the locked variable is stored in the following packed mapping upon minting.  
`mapping(address => uint256) packedEthboxInfo;`

In the minting function...

```
packedEthboxInfo[msg.sender] = packEthboxInfo(
    msg.sender,
    defaultDripTime,
    defaultEthboxSize,
    false // <-- locked variable
  );
```

If you are new to variable packing, start [here](/packing).

The bit position of the locked variable in the uint256 is 224. This is the last variable we store in the uint256.  
A boolean only requires one bit, we set this and shift it into position before adding the final [size](/ethboxes/size/how-it-works) variable like this...

```
uint256 private constant BITPOS_ETHBOX_LOCKED = 224;

//first we need to turn the bool into a number where true = 1 and false = 0
//we use the following function

function boolToUint(bool _b) private pure returns (uint256) {
    uint256 _bInt;
    assembly {
      // SAFETY: Simple bool-to-int cast.
      _bInt := _b
    }
    return _bInt;
}

//once its a number, in the packing function we shift it as such

uint256 otherPackedVariables |= (_locked_ << BITPOS_ETHBOX_LOCKED);
```

We can then access this variable at any time using the following function...

```
function unpackEthboxLocked(address _address) private view returns (bool) {
  uint256 flag = (packedEthboxInfo[_address] >> BITPOS_ETHBOX_LOCKED) &
    uint256(1);
  return flag != 0;
}
```

This function is actually not being used currently and may be removed before deploying. Like size, the locked value is usually accessed using the general unpacking function.
