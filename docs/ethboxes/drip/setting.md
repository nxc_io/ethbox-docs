---
sidebar_position: 2
---

# Setting

After minting, you can change you drip time through the [app](https://ethbox.nxc.io/) or [contract](https://etherscan.io/address/0x2b74fbe86367056ff07d0f0fc05539b1afb61038#writeProxyContract).

**Conditions for changing your drip time**

- Your ethbox must be empty. Read more about removing/emptying your ethbox [here](/messages/removing).
  - You can [lock](/ethboxes/locking) after empyting your ethbox to ensure no messages come in before you change your drip time.

**Why would I want to change my drip time?**

- Decreasing it will allow you to claim ether from future messages faster.
- Increasing it may incentivise people to send higher value messages as they last longer.

**Implementation**

The contract function that handles updating drip times...

```
function changeEthboxDripTime(uint256 _dripTime) external {
    EthboxStructs.EthboxInfo memory ethboxInfo = unpackEthboxInfo(msg.sender);
    require(ethboxInfo.size != 0, "ethbox needs to be minted");
    require(ethboxMessages[msg.sender].length == 0, "ethbox needs to be empty");

    packedEthboxInfo[msg.sender] = packEthboxInfo(
      ethboxInfo.recipient,
      _dripTime,
      ethboxInfo.size,
      ethboxInfo.locked
    );
  }
```

We first need to unpack the owner's ethbox information to check they have minted, their inbox is locked, and they have no messages.

If this is the case, we repack the ethbox info with the new drip time.

More about packing [here](/packing).
