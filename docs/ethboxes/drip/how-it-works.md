---
sidebar_position: 1
---

# How it Works

The time it takes for a message to expire in an ethbox.

**General Information**

- The default drip time is **4 weeks**.
- Drip time can be set through the [app](https://ethbox.nxc.io/) or [contract](https://etherscan.io/address/0x2b74fbe86367056ff07d0f0fc05539b1afb61038#writeProxyContract)
- All of a message's value, minus [fees](/fees), are claimable by the owner once it expires.

**Example**

- Sender _X_ sends a message valued at **1 ether** to recipient _Y_.
- _Y_ has their drip time set to **1 day**.
- After **exactly** half a day, sender _X_'s message gets [bumped](/messages/bumping) out of the ethbox by a higher value message.
- Sender _X_ is refunded **0.5 ether** (minus fees) and the sender can [claim](/messages/bumping) the remaining ether at any time.

Not only does drip time determine how long it takes for a message to leave the ethbox - but how much ether is claimable/refunded if that message is [removed](/messages/removing) or [bumped](/messages/bumping).

**Implementation**

Default drip time is set in the contract as such...  
`uint256 public constant defaultDripTime = 4 weeks;`

This variable is then stored in the following packed mapping upon minting.  
`mapping(address => uint256) packedEthboxInfo;`

In the minting function...

```
packedEthboxInfo[msg.sender] = packEthboxInfo(
    msg.sender,
    defaultDripTime,
    defaultEthboxSize,
    false
  );
```

If you are new to variable packing, start [here](/packing).

The bit position of the drip timestamp in the uint256 is 160, right after the recipient address.  
We set this and shift the timestamp into position like this...

```
uint256 private constant BITPOS_ETHBOX_DRIP_TIMESTAMP = 160;

//inside the packing function

uint256 packedAddress |= (_newDrip << BITPOS_ETHBOX_DRIP_TIMESTAMP)

//we pack other variables into this afterwards as well
```

We can then access this variable at any time using the following function...

```
function unpackEthboxDrip(address _address) private view returns (uint64) {
    return uint64(packedEthboxInfo[_address] >> BITPOS_ETHBOX_DRIP_TIMESTAMP);
  }
```

Drip time is used to calculate how much of a message's value is claimable at any given time, see more [here](/messages/claiming).
