---
sidebar_position: 1
---

# How it Works

The maximum amount of messages an ethbox can contain.

**General Information**

- The default ethbox size is 3.
- You can upgrade your size for a fee. This fee increases as you buy more slots.
- Senders compete for a slot in your inboxes by bidding ether.

**If an inbox is not full...**

- Anyone can send a message, without paying any ether. They will just be pushed into the inbox.

**If an inbox is full...**

- The sender will need to bid a larger value of eth than lowest value existing message.
- The existing message will be [bumped](/messages/bumping) out of the ethbox.

**You can [upgrade](/ethboxes/size/upgrading) your ethbox size.**

Ethbox slots get more expensive the more you buy.

![Table](../../../images/size-table.png)

**Implementation**

Default ethbox size is set in the contract as such...  
`uint256 public constant defaultEthboxSize = 3;`

Just like drip time, this variable is stored in the following packed mapping upon minting.  
`mapping(address => uint256) packedEthboxInfo;`

In the minting function...

```
packedEthboxInfo[msg.sender] = packEthboxInfo(
    msg.sender,
    defaultDripTime,
    defaultEthboxSize,
    false
  );
```

If you are new to variable packing, start [here](/packing).

The bit position of the size variable in the uint256 is 225. This is the last variable we store in the uint256.
We set this and shift the timestamp into position like this...

```
uint256 private constant BITPOS_ETHBOX_SIZE = 225;

//in the packing function

uint256 otherPackedVariables |= (_newSize << BITPOS_ETHBOX_SIZE);
```

We can then access this variable at any time using the following function...

```
function unpackEthboxDrip(address _address) private view returns (uint64) {
    return uint64(packedEthboxInfo[_address] >> BITPOS_ETHBOX_DRIP_TIMESTAMP);
  }
```

This specific function is mainly used in our tokenURI function to generate the SVG [metadata](/metadata).

We usually use the general unpacking function when accessing size, because it is often useful with the other packed variables.

Specifically, when sending a message, we need to check the number of messages an ethbox has received vs their ethbox size to see if we need to bump a message out. i.e.

```
if (numberOfMessages < ethboxInfo.size) {
    _pushMessage(_to, msg.sender, _message, msg.value, dynamicBoxSize);
}
```

In the case where the number of messages is less than the ethbox size, we can just push the message.

Otherwise, we run a series of checks to see if the message qualifies (is high enough value), and which other message to replace. A simplified version of the code...

```
if (qualifies) {
    _insertMessage(newMessage, oldMessage);
} else {
    revert("message value too low");
}
```
