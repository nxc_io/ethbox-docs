---
sidebar_position: 2
---

# Upgrading

After minting, you can upgrade your ethbox size through the [app](https://ethbox.nxc.io/) or [contract](https://etherscan.io/address/0x2b74fbe86367056ff07d0f0fc05539b1afb61038#writeProxyContract).

**Why would I want to increase my ethbox size?**

- The more messages you can have in your ethbox, the more ether you can yield at any given time.

**Why would I not want to increase my ethbox size?**

- A very large ethbox may make your ethbox slots uncompetitive.

**Implementation**

The default ethbox size increase fee and size increase BPS are set as such...

```
uint256 public sizeIncreaseFee = 0.05 ether;
uint256 public sizeIncreaseFeeBPS = 2500;
```

Both are updatable by the contract owner using the following functions...

```
function setSizeIncreaseFee(uint256 _sizeIncreaseFee) external onlyOwner {
    sizeIncreaseFee = _sizeIncreaseFee;
}

function setSizeIncreaseFeeBPS(uint256 _sizeIncreaseFeeBPS)
    external
    onlyOwner
{
    sizeIncreaseFeeBPS = _sizeIncreaseFeeBPS;
}
```

We calculate the increase fee for a given ethbox as such...

Where `_size` is the new size of the ethbox.

```
function calculateSizeIncreaseCost(uint256 _size, uint256 _currentSize)
    public
    view
    returns (uint256 total)
{
  require(_size > _currentSize, "new size should be larger");
  total = 0;
  for (uint256 i = _currentSize; i < _size; i++) {
    if (i == defaultEthboxSize) {
      total += sizeIncreaseFee;
    } else {
      total +=
        (sizeIncreaseFee *
          ((sizeIncreaseFeeBPS + 10000)**(i - defaultEthboxSize))) /
        (10000**(i - defaultEthboxSize));
    }
  }
  return total;
}
```

Once the size increase cost is calculated we update the ethbox's size with the following function.

```
function changeEthboxSize(uint256 _size) external payable {
    EthboxStructs.EthboxInfo memory ethboxInfo = unpackEthboxInfo(msg.sender);
    require(ethboxInfo.size != 0, "ethbox needs to be minted");
    uint256 total = calculateSizeIncreaseCost(_size, ethboxInfo.size);
    require(total == msg.value, total.toString());

    packedEthboxInfo[msg.sender] = packEthboxInfo(
      ethboxInfo.recipient,
      ethboxInfo.drip,
      _size,
      ethboxInfo.locked
    );
    (bool successFees, ) = messageFeeRecipient.call{ value: msg.value }("");
    require(successFees);
}
```

Just like changing an ethbox's [drip](/ethboxes/drip/setting), we need to unpack and repack the variables with the new value.

More about packing [here](/packing).
