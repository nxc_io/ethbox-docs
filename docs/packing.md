---
sidebar_position: 7
---

# Packing

Note: This will be difficult to understand without prior solidity or programming experience.

Packing variables in a struct is way to ensure that the minimum amount of storage slots are used for any given data. This saves gas as less storage reads are required to complete an operation.

Take the following struct `EthboxSettings` from the `EthboxStructs` library.

```
struct EthboxSettings {
  uint256 size;
  uint256 drip;
  address payoutRecipient;
  bool isLocked;
}
```

Each storage slot in solidity can be occupied by 256 bits of data. In the `EthboxSettings` struct...

- `size` and `drip` are taking up one storage slot each.
- `payoutRecipient` and `isLocked` share the same storage slot.
  - An address in solidity occupies 160 bits and a boolean occupies 8, so these can fit in one together.

There are two initial things to note...

1. Since increasing the [size](/ethboxes/size/how-it-works) of an ethbox gets exponentially more expensive with every slot, we can safely assume the number will never become big. Therefore, we can fit it in a `uint8`.

2. `drip` is a timestamp and timestamps (currently) only require 48 bits of storage. However, for a perhaps unneeded amount of future-proofness I prefer setting them to a `uint64`.

Our struct now looks like this.

```
struct EthboxSettings {
  uint8 size;
  uint64 drip;
  address payoutRecipient;
  bool isLocked;
}
```

Now our struct only occupies one storage slot as `8 + 64 + 160 + 8 < 256`.

We can make this even better by packing the struct into a single uint256. One of the reasons this is preferred is that reading from a uint256 mapping is cheaper than reading from a struct mapping.

Take the test struct `TestStruct` for example, and a mapping from `address => TestStruct` and a mapping from `address => uint256`.

```
struct TestStruct {
        uint256 value;
}

mapping(address => TestStruct) structMapping;
mapping(address => uint256) uintMapping;
```

The following table illustrates the gas savings from using a uint256 mapping.

|           | Struct Mapping | Uint256 Mapping |
| --------- | :------------: | :-------------: |
| Read Gas  |     24366      |      24273      |
| Write Gas |     24300      |      24119      |

So, we should convert our `EthboxSettings` into a single uint256. These gas savings increase as more fields are added to the struct.

This is not the only benefit. Though a boolean takes up 8 bits in a storage slot, by converting it to a uint256 we it can only take up one.

- This frees up space in the uint256 for other data.

Using inline assembly, the following function converts a boolean value to a uint256 where a true value returns 1, and a false value returns 0.

```
function boolToUint(bool _b) private pure returns (uint256) {
  uint256 _bInt;
  assembly {
    _bInt := _b
  }
  return _bInt;
}
```

We can now start packing all of these values into a single uint256.

The first step is to lay out the bit positions of each variable.

- An address takes up 160 bits. So `payoutRecipient` will occupy bits 0-159.
- The timestamp takes up 64 bits, so `drip` will occupy bits 160-223
- The `locked` variable only takes up 1 bit and will occupy bit 224.
- Finally, the `size` takes up 8 bits and will occupy bits 225-232

So the final bit layout looks like this.

|      | payoutRecipient |  drip   | locked |  size   |
| ---- | :-------------: | :-----: | ------ | :-----: |
| Bits |      0-159      | 160-223 | 224    | 225-232 |

Now let's declare these as variables...

```
uint256 private constant BITPOS_ETHBOX_DRIP_TIMESTAMP = 160;
uint256 private constant BITPOS_ETHBOX_LOCKED = 224;
uint256 private constant BITPOS_ETHBOX_SIZE = 225;
```

It is also important that we mask bits in the `payoutRecipient` variable after turning it in to a uint256 just in case they are "dirty". If the upper 96 bits (256 - 160) are dirty, they will mess up the insertion of the other variables. Credits to Azuki's ERC721A for this.

So we start by turning the address into a uint256 and masking it as such, using the & operator.

```
uint256 private constant BITMASK_RECIPIENT = (1 << 160) - 1;

uint256 packedEthbox = uint256(uint160(_recipient)) & BITMASK_RECIPIENT;
```

Now we can start adding in the other variables using the shift left (<<) operator.

```
packedEthbox |= _drip << BITPOS_ETHBOX_DRIP_TIMESTAMP
```

The same process applies for the other variables. For the locked variable we use our `boolToUint` function we looked at earlier as well.

```
packedEthbox |= (boolToUint(_locked) << BITPOS_ETHBOX_LOCKED)
```

Finally, we added our size.

```
packedEthbox |=  (_size << BITPOS_ETHBOX_SIZE);
```

We can do all of this nicely in one line using the | operator. The final function looks like this.

```
function packEthboxInfo(
  address _recipient,
  uint256 _drip,
  uint256 _size,
  bool _locked
) private pure returns (uint256) {
  uint256 packedEthbox = uint256(uint160(_recipient)) & BITMASK_RECIPIENT;
  packedEthbox |=
    (_drip << BITPOS_ETHBOX_DRIP_TIMESTAMP) |
    (boolToUint(_locked) << BITPOS_ETHBOX_LOCKED) |
    (_size << BITPOS_ETHBOX_SIZE);
  return packedEthbox;
}
```

It follows that unpacking these variables uses the reverse process. For example, to unpack the drip timestamp...

```
uint64 unpackedTimestamp = uint64(packedEthboxInfo[_address] >> BITPOS_ETHBOX_DRIP_TIMESTAMP)
```

We can also unpack the entire uint256 into the `EthboxSettings` struct as such...

```
function unpackEthboxInfo(address _address)
    public
    view
    returns (EthboxStructs.EthboxInfo memory ethbox)
{
  uint256 packedEthbox = packedEthboxInfo[_address];
  ethbox.recipient = address(uint160(packedEthbox));
  ethbox.drip = uint64(packedEthbox >> BITPOS_ETHBOX_DRIP_TIMESTAMP);
  ethbox.size = uint8(packedEthbox >> BITPOS_ETHBOX_SIZE);
  ethbox.locked = ((packedEthbox >> BITPOS_ETHBOX_LOCKED) & uint256(1)) != 0;
}
```

Note: For the locked value we need to convert the number back into a boolean. We do this by checking if it does not equal 0. If it is 0, it does equal 0, so we return false as expected. If it is 1, it does not equal 0 so we return true, also as expected.

Note: The same exact process applies for the packing we do in the "data" field of the `Message` struct.
