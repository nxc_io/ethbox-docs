---
sidebar_position: 2
---

# ENS Resolution

Using the reverse registrar, we resolve ENS names from address to display on their SVG.

NOTE: Reverse resolution is not always accurate. Anyone can set their address to resolve to any ENS name. Meaning, the ENS displayed in the OpenSea SVG may be inaccurate. The address' real ENS will be displayed on the [app](https://ethbox.nxc.io).

**Implementation**

First we prepare the needed contracts.

- The reverse registrar `node` function returns a `bytes32` string called a node.
- The ENS `resolver` function takes in this node and returns the resolver address for the ENS name.
- The resolver `name` function returns the name using this node.

```
abstract contract ReverseRegistrar {
  function node(address addr) public pure virtual returns (bytes32);
}

abstract contract ENS {
  function resolver(bytes32 node) public view virtual returns (address);
}

abstract contract Resolver {
  function name(bytes32 node) public view virtual returns (string memory);
}
```

Then, we instantiate these contracts as such...

```
ReverseRegistrar reverseRegistrar = ReverseRegistrar(0x084b1c3C81545d370f3634392De611CaaBFf8148)
ENS ens = NS(0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e)
```

The `addressToENS` function handles the resolution.

The first step is to fetch the node and resolver.

```
bytes32 node = reverseRegistrar.node(_address);
address resolverAddress = ens.resolver(node);
```

Then, if the resolver address is the zero address we know this wallet does not have an associated ENS so we just return a string of their address.

```
if (resolverAddress == address(0))
    return Strings.toHexString(uint160(_address), 20);
```

Otherwise, we instantiate the resolver contract and return the name.

```
Resolver resolver = Resolver(resolverAddress);
return resolver.name(node);
```
