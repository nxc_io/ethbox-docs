---
sidebar_position: 1
---

# SVG Composition

Embedding HTML in the SVG allows us to create functioning buttons on opensea to cycle through messages.

Our metadata contract is seperate to our main contract. This allows us to update it and fix bugs.

**Implementation**

First off, we're using the `strings` and `base64` libraries to help with string inserts and encoding the metadata.

The github for `strings` can be found [here](https://github.com/Arachnid/solidity-stringutils)

`strings` allows us to do the following...

- Convert a string into a `slice` struct using the `toSlice()` method, which looks like this...
  ```
   struct slice {
     uint _len;
     uint _ptr;
   }
  ```
- Then we can perform `split` operations on this struct, allowing us to lookup a given character or word, and split the string at that point.
- This is necessary for the custom string inserts we need to build up an individual's ethbox.

To elaborate further, check out the following function.

```
function splitString(
    string memory _stringToSplit,
    string memory _splitAtString
  ) public pure returns (string memory, string memory) {
    strings.slice memory sliceToSplit = _stringToSplit.toSlice();

    string memory output = sliceToSplit
      .split(_splitAtString.toSlice())
      .toString();

    return (output, sliceToSplit.toString());
  }
```

This function takes in the string we want to split, and the string we want to split it at. First we are turning the string we want to split into a slice, so that it is "splittable". Then we are splitting the string by the slice of the string we want to split it at. Finally we convert all of that back into a string. Output will now contain everything **before** the string we split `sliceToSplit` at, and `sliceToSplit` will store everything **afterwards**.

Example:

- Input - `_stringToSplit = "hello world"` and `splitAtString = "wor"`
- Output - `output = "hello"` and `sliceToSplit.toString() = "ld"`

As you can see, the string we want to split by is removed, the preceding characters are stored in output and the characters afterwards are stored in `sliceToSplit`.

The main SVG building function in the contract is `buildSVG`

```
function buildSVG(
  address _to,
  EthboxStructs.UnpackedMessage[] memory _messages,
  uint256 _maxSize
) private view returns (string memory) {...}
```

The first step is to turn the `ethboxInsert` set in the constructor into a slice. The `ethboxInsert` is simply the SVG string we use to wrap the content (messages) of the ethbox.

```
strings.slice memory sliceEthbox = ethboxInsert.toSlice();
```

Inside this string insert, we use Javascript style insert strings to split it up as such.

```
string memory output = sliceEthbox
    .split("${_MESSAGES}".toSlice())
    .toString();
```

Now in memory we are storing everything before where we want the messages to go in output, and everything afterwards in `sliceEthbox`.

Before we start building the SVG, we set a variable called `visibility` to "visible".

```
string memory visibility = "visible";
```

This is a variable used in the SVG construction to determine which message is visible. Before cycling through messages, only the first one is visible.

Next, we need to check if the ethbox actually has messages. If it doesn't we build the empty ethbox SVG.

```
if (_messages.length == 0) {
  output = string(abi.encodePacked(output, buildEmptyEthboxSVG(_to)));
} else {...}
```

This is done by using the empty ethbox insert set in the constructor. We split the insert at `{_TO}` and insert the output of `addressToENS`. Read more about this function [here](/metadata/ens).

```
function buildEmptyEthboxSVG(address _to)
    private
    view
    returns (string memory)
{
  strings.slice memory sliceMessage = emptyInsert.toSlice();

  string memory output = string(
    abi.encodePacked(

      //everything before ${_TO}
      sliceMessage.split("${_TO}".toSlice()).toString(),

      //the address or ENS
      addressToENS(_to),

      //everything after ${_TO}
      sliceMessage.toString()
    )
  );

  return output;
}
```

If the ethbox has messages, we use a for loop to build their respective SVGs.

As soon as we get to the second message, if there is one, we set the SVG visibility to "none" so that only the first message is initially visible.

```
for (uint256 i = 0; i < _messages.length; i++) {
  if (i == 1) {
    visibility = "none";
  }
  EthboxStructs.MessageInfo memory display = EthboxStructs.MessageInfo(
    _to,
    visibility,
    _messages[i],
    i,
    _maxSize
  );
  output = string(
    abi.encodePacked(output, buildSingleMessageSVG(display))
  );
}
```

Then, we pass all relevant information about the message into `buildSingleMessageSVG`.

At every stage of the encoding we split the `messageInsert`. First, we split it at "${\_INDEX}" and insert the index of the message. Then "${\_VISIBILITY}" and so on and so forth.

```
function buildSingleMessageSVG(EthboxStructs.MessageInfo memory _display)
    private
    view
    returns (string memory)
{
  strings.slice memory sliceMessage = messageInsert.toSlice();

  string memory output = string(
    abi.encodePacked(
      sliceMessage.split("${_INDEX}".toSlice()).toString(),
      Strings.toString(_display.index),
      sliceMessage.split("${_VISIBILITY}".toSlice()).toString(),
      _display.visibility,
      sliceMessage.split("${_TO}".toSlice()).toString(),
      addressToENS(_display.to),
      sliceMessage.split("${_FROM}".toSlice()).toString(),
      addressToENS(_display.message.from),
      sliceMessage.split("${_MESSAGE}".toSlice()).toString()
    )
  );

  output = string(
    abi.encodePacked(
      output,
      _display.message.message,
      sliceMessage.split("${_VALUE}".toSlice()).toString(),
      valueToEthString(_display.message.originalValue),
      sliceMessage.split("${_POSITION}".toSlice()).toString(),
      Strings.toString(_display.index + 1),
      "/",
      Strings.toString(_display.maxSize),
      sliceMessage.toString()
    )
  );

  return output;
}
```

Note: We have to split up the encoding here because `abi.encodePacked` has a maximum number of arguments.

Finally, we join the `output`, which was everything before "${\_MESSAGES}" with the message content we have just created and add back `slicedEthbox` to the end.

```
//this is in every iteration of the for loop
output = string(
    abi.encodePacked(output, buildSingleMessageSVG(display))
  );

return string(abi.encodePacked(output, sliceEthbox.toString()));
```
