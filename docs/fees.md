---
sidebar_position: 6
slug: /fees
---

# Fees

A fee is paid to the contract any time a message is sent.

**General Information**

- The fee is set to 2.5% and is updateable by the contract owner.
- It is taken immediately on message send.

**Implementation**

The fee is initially set in BPS as such, `uint256 public messageFeeBPS = 250`.

Fees can be set using the following function.

```
function setMessageFeeBPS(uint256 _messageFeeBPS) external onlyOwner {
    messageFeeBPS = _messageFeeBPS;
}
```

Fees are payed utlising the `_payFees` function.

```
function _payFees(uint256 _value) private {
    (bool successFees, ) = messageFeeRecipient.call{
      value: (_value * messageFeeBPS) / 10000
    }("");
    require(successFees);
}
```

The recipient of these fees can also be set as such...

```
function setMessageFeeRecipient(address _messageFeeRecipient)
    external
    onlyOwner
{
  messageFeeRecipient = _messageFeeRecipient;
}
```

Fees are payed immediately in the `sendMessage` function.
