---
sidebar_position: 1
slug: /
---

# Introduction

**What is Ethbox?**

_Bid to message anyone._

Ethbox is an Ethereum messaging protocol that enables any wallet to bid ETH to send messages to another wallet.

Ethbox is the world’s first peer to peer attention marketplace, allowing users to bid on attention that is valuable to them, and monetize their attention if it is valuable to others.

Thanks to ENS, Ethereum wallets are now connected directly to identities. In the Ethbox app, every ENS domain resolves to its connected wallet so you can be sure you are reaching the wallet you intend to.

**Ethbox will enable:**

- Friends to send messages to one another
- Anonymous traders to contact one another for exchange
- Fans to bid on the attention of their favorite celebrities
- Artists to bid on the attention of labels, agents and collaborators
- Communities to assemble around words, phrases and memes
- Job applicants to message the company and executives of the business directly
- And much, much more

Ethbox’s infrastructure is 100% on-chain, allowing for composition, derivatives and other emergent use-cases.

**How does it work?**

Each Ethbox is represented by a free, soulbound NFT which any Ethereum wallet can mint. This NFT live [renders](/metadata/svg) the contents of every Ethbox wherever the NFT is displayed (e.g. OpenSea).

Ethboxes have a set number of message slots (default 3) which senders must bid in ETH to fill.

**Messages**

A [message](/messages) consists of:

- A recipient’s address or [ENS](/metadata/end)
- A “bid” value in ETH
- A message (max 140 characters)

Messages are ranked in the inbox by their bid value, and new messages can “bump” older ones by outbidding them.

**Claim**

Each Ethbox has a message lifetime (default 4 weeks). The message lifetime determines the emissions rate of the ETH in a given message, [“dripping”](/ethboxes/drip/how-it-works) value to the owner.

The owner can periodically [claim](/messages/claiming) any outstanding value greater than 1% of the message’s bid.

- At 2 weeks, 50% of the bid may be claimed by the Ethbox owner
- At 4 weeks, 100% of the bid may be claimed by the Ethbox owner

A message can fall out of an inbox by being [outbid](/messages/bumping), or emitting 100% of its bid.

If outbid, all unlocked ETH up to that point can be claimed by the inbox owner, with the remainder being refunded to the message sender.

If the message has emitted all of its ETH, the owner can claim that value and effectively remove it from their ethbox, making room for more messages.
