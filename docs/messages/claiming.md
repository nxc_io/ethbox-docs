---
sidebar_position: 4
---

# Claiming

At any time an owner can claim a portion of a message in their ethbox.

The claimable value of a given message is calculated as such.

- First we deduct fees from the original value.
- Next, we calculate how much time has elapsed since the message was sent
- Multiplying the original value minus fees by the elapsed time/[drip](/ethboxes/drip/how-it-works) time gives us our result.

The ethbox owner has the option to claim the ether from one message, or all of them.

**Implementation**

The breadth of the implementation is handled by the `getClaimableValue` function.

First we unpack the timestamp the message was sent at, and deduct it from the `block.timestamp`

```
uint256 elapsedSeconds = block.timestamp - unpackMessageTimestamp(_message.data);
```

Next, we check run a couple checks on the elapsed seconds.

- If no time has passed, no ether is claimable and we return 0.
- If more time has passed than the [drip](/ethboxes/drip/how-it-works) time of the inbox, we return all of the remaining value in the message.
  - Note: the remaining value is the original message value minus how much ether has already been claimed by the owner.

```
if (elapsedSeconds < 1) return 0;
if (elapsedSeconds > _drip) return getRemainingOriginalValue(_message);
```

Finally, we calculate the BPS by divided the `elapsedSeconds` by the ethbox's `_drip`, deduct fees and any ether that has already been claimed by the owner.

```
uint256 bps = (elapsedSeconds * 100) / _drip;
uint256 subValue = (getOriginalValueMinusFees(_message) * bps) / 100;
return (subValue - _message.claimedValue);
```

The `getClaimableValue` function serves both the `claimAll` and `claimOne` functions.

In the claim functions, we call `getClaimableValue` and update the total ether claimed from the message as such. However, in `claimAll` we also add `bumpedClaimValue`. Read more about this [here](/messages/bumping).

```
uint256 claimValue = getClaimableValue(
  ethboxMessages[msg.sender][_index],
  unpackEthboxDrip(msg.sender)
);

ethboxMessages[msg.sender][_index].claimedValue += claimValue;
_payRecipient(msg.sender, claimValue);
```

Finally, we pay the ethbox recipient the value.

```
_payRecipient(msg.sender, claimValue);
```

Note: The ethbox recipient does not need to be the ethbox owner. This allows ethbox owners to delegate their earnings to another wallet.

The key difference between the `claimAll` and `claimOne` functions is that `claimOne` takes a specific message index to claim, whereas `claimAll` iterates through all the messages and claims each one.
