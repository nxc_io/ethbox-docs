---
sidebar_position: 1
---

# Sending

Send a message to any address or ENS using our [app](https://ethbox.nxc.io/) or [contract](https://etherscan.io/address/0x2b74fbe86367056ff07d0f0fc05539b1afb61038#writeProxyContract).

**How it Works**

Sending a message through our [app](https://ethbox.nxc.io/) is fairly intuitive, but there are a few things to note.

- Every ethbox will have a set [drip](/ethboxes/drip/how-it-works) time, the default is four weeks. Any message you send will expire after this time.
- If an ethbox is full you will need to outbid the other messages in the box. The default ethbox [size](/ethboxes/size/how-it-works) is three.
- If your message gets [bumped](/messages/bumping) or [removed](/messages/removing), you will be refunded any [unclaimed](/messages/claiming) ether.
- The maximum message length is 141 characters.

**Example**

- An inbox with [size](/ethboxes/size/how-it-works) 3 is full.
- Message one contains 3 ether, message two contains 2 ether and message three contains 1 ether.
- To get a spot in this inbox, the next sender must send more than 1 ether.
- Sender _X_ sends a 1.5 ether message.
- Message three gets [bumped](/messages/bumping) out of the inbox and the sender is refunded any [unclaimed](/messages/claiming) ether.

**Implementation**

This is the function that handles sending messages to an ethbox.

```
function sendMessage(
  address _to,
  string calldata _message,
  uint256 _drip
) public payable nonReentrant onlyProxy {...}
```

First we check the message is not too long.

```
require(bytes(_message).length < maxMessageLen, "message too long");
```

Next we unpack the ethbox settings which we can use to check if the ethbox has been minted. If the ethbox has not been minted we use default values for it's drip and size as such.

```
EthboxStructs.EthboxInfo memory ethboxInfo = unpackEthboxInfo(_to);

uint256 compBoxSize = defaultEthboxSize;
uint256 compDrip = defaultDripTime;

//we know the ethbox has not been minted if size == 0.
if (ethboxInfo.size != 0) {
  compBoxSize = ethboxInfo.size;
  compDrip = ethboxInfo.drip;
}
```

Then we make a few checks.

- We check if the ethbox is locked. If it is, the message cannot be sent and the transaction fails.

- We check that the value is either 0 or above 10000 wei. This is for the sake of our BPS calculations.

- We check that the drip inputted into the function is the same as the drip set by the ethbox. We do this due to the following attack vector.

  - Sender _X_ sends a 1 ether message to recipient _Y_ at an expected one day drip time.
  - Recipient _Y_ spots this message in the mempool and does the following.
    - Calls `removeAll()` in the contract.
    - This allows _Y_ to change their drip time using `changeEthboxDripTime`.
    - _Y_ changes their drip time to a very low value to drain this message's ether quicker than _X_ expected.
    - Throws these transactions in a bundle with _X_'s `sendMessage()` call coming last.
  - In short, without a check here, a recipient could change their drip time as a message is being sent.

```
require(!ethboxInfo.locked, "ethbox is locked");
require(msg.value > 10000 || msg.value == 0, "message value incorrect");
require(compDrip == _drip, "message drip incorrect");
```

Next we check if the ethbox is full, if it isn't we can just push the message to the array regardless of its value.

```
uint256 dynamicBoxSize = toMessages.length;

if (dynamicBoxSize < compBoxSize) {
  _pushMessage(_to, msg.sender, _message, msg.value, dynamicBoxSize);
} else {...}
```

Otherwise, we need to check if the value of the message is enough to enter the box. The breadth of this is handled by our `_getIndexToReplace` function. It returns (1) whether the message even qualifies to be in the ethbox and (2) if it does, what is the index of the lowest value existing message to replace.

```
else {
  (bool qualifies, uint256 indexToRemove) = _getIndexToReplace(
    toMessages,
    dynamicBoxSize,
    msg.value
  );
  if (qualifies) {
    EthboxStructs.Message memory droppedMessage = toMessages[indexToRemove];
    uint256 claimValue = getClaimableValue(droppedMessage, compDrip);
    droppedMessage.claimedValue += claimValue;
    bumpedClaimValue[_to] += claimValue;
    _refundSender(droppedMessage);
    _insertMessage(_to, msg.sender, _message, msg.value, indexToRemove);
  } else {
    revert("message value too low");
  }
}
```

Note how we refund the sender of the `droppedMessage`, read about [bumping](/messages/bumping) to learn more. We also update the ethbox owner's claimable value using a `bumpedClaimValue` message. This value depends on how long the message has been in the ethbox. The owner can claim this ether whenever they desire through our [app](https://ethbox.nxc.io) or [contract](https://etherscan.io/address/0x2b74fbe86367056ff07d0f0fc05539b1afb61038#writeProxyContract).

At the end of the function, the contract owner takes an immediate [fee](/fees) determined by the `messageFeeBPS`
