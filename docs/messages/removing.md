---
sidebar_position: 2
---

# Removing

If an owner doesn't like a message in their inbox, they can remove it using our [app](https://ethbox.nxc.io/) or [contract](https://etherscan.io/address/0x2b74fbe86367056ff07d0f0fc05539b1afb61038#writeProxyContract).

**How it works**

- Sender _X_ sends a message to recipient _Y_ with value 1 ether, at a drip time of one day.
- After **exactly** half a day, _Y_ sees the message and does not like it.
- _Y_ removes the message from their inbox, claiming half of the ether and refunding the other half to _X_.

**Why would someone want to remove a message**

- The message may be inappropriate. Since ethboxes are public a user may not want everyone to be able to see that in their box.
- They may be preparing to change their [drip](/ethboxes/drip/how-it-works) time.

Of course there is an opportunity cost of removing a message. Though you get rid of the message you don't like, you forgo a portion of the ether that message contained.

Only ethbox owners can remove messages. Senders cannot "unsend" their message and recoup their ether.

**Implementation**

The `_removeMessage` function is used to remove messages.

The first step is grabbing the message array and moving the message to delete to the end of it as such.

```
EthboxStructs.Message[] memory messages = ethboxMessages[_to];
ethboxMessages[_to][_index] = messages[messages.length - 1];
```

In the same way that we pack ethbox settings for [drip](/ethboxes/drip/how-it-works), [size](/ethboxes/size/how-it-works) and [locked](/ethboxes/locked) we pack the field `data` in the message struct.

```
struct Message {
    string message;
    uint256 originalValue;
    uint256 claimedValue;
    uint256 data;
}

//data stores the following

struct MessageData {
    address from;
    uint64 timestamp;
    uint8 index;
    uint24 feeBPS;
}
```

Ethbox structs are stored in a library called `EthboxStructs`.

Next we update the index of the message we are swapping the place of the removed message with. We use unpacking and repacking to do this.

```
EthboxStructs.MessageData memory messageData = unpackMessageData(
      ethboxMessages[_to][_index].data
    );

ethboxMessages[_to][_index].data = packMessageData(
    messageData.from,
    messageData.timestamp,
    _index,
    messageData.feeBPS
);
```

Finally, we pop the message.

```
ethboxMessages[_to].pop();

```
