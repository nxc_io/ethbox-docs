---
sidebar_position: 3
---

# Bumping

If an ethbox is full, an incoming message with a higher value than at least one of the existing messages will bump the lowest value message out of the inbox.

**Example**

- Ethbox owner _Y_ has three messages in their ethbox.
  - Message one has 3 ether.
  - Message two has 2 ether.
  - Message three has 1 ether.
- Incoming message **M1** contains 0.75 ether.
  - The transaction will revert as the message does not contain enough ether to compete in the box.
- Incoming message **M2** contains 1.5 ether.
  - **M2** will bump out message three and any remaining ether contained will be refunded to the sender. The rest is claimable to the owner. **M2** will be displayed at the bottom of the ethbox as it is now the lowest value message.
- Finally, incoming message **M3** contains 4 ether.
  - **M3** will bump out **M2** and is now the highest value message in the box. Therefore, it will be displayed at the top of the ethbox.

**Implementation**

The implementation for bumping is essentially the same as [removing](/messages/removing).

However, there is one key difference. When a message is removed by an owner, the owner instantly claims any claimable ether. Read about [claiming](/messages/removing) to learn more about how this claimable value is calculated.

When a message is bumped, the claimable value is stored in a mapping called `bumpedClaimValue`.

```
mapping(address => uint256) public bumpedClaimValue;
```

In [send](/messages/sending) message, when a message is bumped, this is incremented by the claimable value as such...

```
bumpedClaimValue[_to] += claimValue;
```

Then, when calling `claimAll`, this value is added it the total as such...

```
totalValue += bumpedClaimValue[msg.sender];
```
